# CAD File Repository

Repository for FCC-ee Interaction Region CAD files.  

The files are organized as follows:

## FCC-ee IF 
Latest update: MTG 02/12/2020
Present versin similar to V0.0 but with simplified versions of the coils to save space (now they appear as cylinders). Also includes minor fixes.

## FCC-ee IR

This is version 0.0 of the interaction region 
- author: Mike Koratzinos
- date: 21/9/2020
This is a conceptual design nd not an engineering version, however there are some inconcictencies and omissions.

Known problems:

- BPMs might need to be closer to QC1L1
- beampipe might change significantly and HOM absorbers might not be needed. this will have strong repercussions on many parts of the design
- cryostat for screening solenoid missing. This is because the concept of cold to warm transitions at the supports has not been developed.

## Repository

To create a local copy of the files to a directory use:
```
git clone https://gitlab.cern.ch/fcc-ee-ir/cad.git
```
