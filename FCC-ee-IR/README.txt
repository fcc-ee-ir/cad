This is version 0.0 of the interaction region 

author: Mike Koratzinos
date: 21/9/2020

This is a conceptual design nd not an engineering version

there are some inconcictencies and omissions.

known problems:

* BPMs might need to be closer to QC1L1
* beampipe might change significantly and HOM absorbers might not be needed. this will have strong repercussions on many parts of the design
* cryostat for screening solenoid missing. This is because the concept of cold to warm transitions at the supports has not been developed.

